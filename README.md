---

~~~
注意：以下界面原型代码对应的分支为**dev**分支，不是master分支
~~~

## 个人博客前端界面
## 技术选型
~~~md
- React
- Antd
~~~
## 线上地址
~~~md
http://123.57.152.16/
~~~
## 界面原型
### 首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/135800_0bba0daa_7564012.png "屏幕截图.png")
---
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/135838_9c5aa76d_7564012.png "屏幕截图.png")
---
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/135906_2a18f9ed_7564012.png "屏幕截图.png")
---
### 详情页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/140336_f6ce8379_7564012.png "屏幕截图.png")
---
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/140352_06527852_7564012.png "屏幕截图.png")
---
### 资源
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/140132_47fc2f20_7564012.png "屏幕截图.png")
---
### 留言
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/140206_336df24c_7564012.png "屏幕截图.png")