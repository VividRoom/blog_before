import React from "react";
import ReactDOM from  "react-dom";
import {BrowserRouter} from "react-router-dom"
import axios from "axios";
import App from "./App";

axios.defaults.baseURL = "http://123.57.152.16:80";

axios.defaults.withCredentials = true;

ReactDOM.render(
    <BrowserRouter>
        <App/>
    </BrowserRouter>
    , document.getElementById("root"));



