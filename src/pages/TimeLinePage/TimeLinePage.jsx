import React, {Component} from 'react';
import axios from "axios";
import marked from 'marked';

class TimeLinePage extends Component {

    state = {
        timeLines: []
    }

    componentDidMount() {
        axios("/times?page=1&limit=5").then(response => {
            let  {list} = response.data.data;
            this.setState({timeLines: list});
        })
    }


    goHome = () => {
        let {history} = this.props;
        history.replace("/home");
    }

    render() {

        let {timeLines} = this.state;

        return (
            <>
                <div className="layui-row vi-top">
                    <div className="layui-card">
                        <div className="layui-card-header">
                            <strong onClick={this.goHome} className="vi-point">
                                <i className="layui-icon">&#xe65c;</i>
                            </strong>
                            <strong className="vi-left">
                                <i className="layui-icon">&#xe62c;</i>
                                时间线
                            </strong>
                        </div>
                        <div className="layui-card-body vi-padding">
                            <div className="layui-row">
                                <ul className="layui-timeline">
                                    {
                                        timeLines.map(item => {
                                            return (
                                                <li className="layui-timeline-item" key={item.id}>
                                                    <i className="layui-icon layui-timeline-axis">&#xe63f;</i>
                                                    <div className="layui-timeline-content layui-text">
                                                        <h3 className="layui-timeline-title">{new Date(item.createTime).toLocaleString()}</h3>
                                                        <p  dangerouslySetInnerHTML={{__html: marked(item.content || "")}}></p>
                                                    </div>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default TimeLinePage;