import React, {Component} from 'react';
import iocImg from "./img/6.jpg";

class MyInfo extends Component {

    goHome = () => {
        let {history} = this.props;
        history.replace("/home");
    }

    render() {
        return (
            <div className="layui-col-sm8">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <strong className="vi-point" onClick={this.goHome}>
                            <i className="layui-icon">&#xe65c;</i>
                        </strong>
                    </div>
                    <div className="layui-card-body">
                        <div className="layui-row">
                            <img src={iocImg} alt="" width="100%"/>
                        </div>
                        <div className="layui-row vi-top layui-text">
                            <p>Vivi</p>
                            <p>一个平凡的、不能再平凡的人。</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MyInfo;