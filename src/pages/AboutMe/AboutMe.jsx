import React, {Component} from 'react';
import MyInfo from "./MyInfo/MyInfo";
import MyHobby from "./MyHobby/MyHobby";

class AboutMe extends Component {
    render() {
        let {history} = this.props;
        return (
           <>
           <MyInfo history={history}/>
           <MyHobby/>
           </>
        );
    }
}

export default AboutMe;