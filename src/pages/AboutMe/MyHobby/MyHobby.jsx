import React, {Component} from 'react';

class MyHobby extends Component {


    tip = (msg, id) => {
        /*global layer*/
        layer.tips(msg, id ,{
            tips: [1, '#000000']
        });
    }


    render() {
        return (
            <div className="layui-col-sm4">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe60b;</i> 关于我
                    </div>
                    <div className="layui-card-body layui-text">
                        <div className="layui-row ">
                            <strong>
                                <span>个性签名：</span>
                                <span> 愿你编码半生、归来仍是少年！</span>
                            </strong>
                        </div>
                        <hr className="layui-bg-cyan"/>
                        <p className="vi-bottom">个人爱好：</p>
                        <div className="layui-row ">
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-orange">编程</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-gray">写作</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-green">思考</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-red">看片</span>
                            </div>
                        </div>
                        <hr className="layui-bg-cyan"/>
                        <p className="vi-bottom">技能点：</p>
                        <div className="layui-row vi-bottom">
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-red">Java</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-green">JavaScript</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-gray">React</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-orange">SpringBoot</span>
                            </div>
                        </div>
                        <div className="layui-row">
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-cyan">看片</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge layui-bg-blue">mysql</span>
                            </div>
                            <div className="layui-col-sm3">
                                <span className="layui-badge-dot"> </span>
                                <span className="layui-badge-dot layui-bg-orange"> </span>
                                <span className="layui-badge-dot layui-bg-green"> </span>
                                <span className="layui-badge-dot layui-bg-cyan"> </span>
                                <span className="layui-badge-dot layui-bg-blue"> </span>
                                <span className="layui-badge-dot layui-bg-black"> </span>
                                <span className="layui-badge-dot layui-bg-gray"> </span>
                            </div>
                        </div>
                        <hr className="layui-bg-cyan"/>
                        <p className="vi-bottom">联系我：</p>
                        <div className="layui-row vi-bottom ">
                            <div className="layui-col-sm4">
                                <button type="button"
                                        id="wc"
                                        onClick={() => this.tip("猫屎咖啡", "#wc")}
                                        className="layui-btn layui-btn-sm layui-btn-primary">
                                    <i className="layui-icon">&#xe677;</i>
                                </button>
                            </div>
                            <div className="layui-col-sm4">
                                <button type="button"
                                        id="qq"
                                        onClick={() => this.tip("QQ：2500247684", "#qq")}
                                        className="layui-btn layui-btn-sm layui-btn-primary">
                                    <i className="layui-icon">&#xe676;</i>
                                </button>
                            </div>
                            <div className="layui-col-sm4">
                                <button type="button"
                                        id="iphone"
                                        onClick={() => this.tip("13233039519", "#iphone")}
                                        className="layui-btn layui-btn-sm layui-btn-primary">
                                    <i className="layui-icon">&#xe680;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MyHobby;