import React, {Component} from 'react';
import axios from "axios";

class Register extends Component {


    state = {
        count: 0,
    }

    sendEmailCode = () => {
        let {count} = this.state;
        count = count + 1;
        this.setState({count});
        if (this.state.count < 3) {
            let myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
            let email = this.refs.code.value;
            if (myreg.test(email)) {
                axios.post("/messages/email/" + email).then(response => {
                    let {status} =response.data;
                    if (status) {
                        /*global layer*/
                        layer.alert("验证码已发送、请注意查收~~", {icon: 6, title: false});
                        return;
                    }
                    layer.alert("发送失败、网络异常、请稍后再试~~", {icon: 5, title: false});
                });
                return;
            }
            layer.alert("邮箱格式不正确~~", {icon: 5, title: false});
            return;
        }
        layer.alert("抱歉、验证码发送过于频繁、请稍后再试", {icon: 5, title: false});
    }


    register = () => {
        let {history} = this.props
        /*global layui*/
        layui.use(['form', 'layer'], function(form, layer){
            form.on('submit(register)', function(data){
                let {code, content} = data.field;
                if (content.trim().length > 300) {
                    layer.alert("抱歉、留言字数过多、请精简！", {icon: 5, title: false});
                    return false;
                }
                axios.post("/messages/register/"+code, data.field).then(response => {
                    let {status, message} = response.data;
                    if (status) {
                        layer.alert("注册成功~~", {icon : 6, title: false}, function (index) {
                            history.replace("/message");
                            layer.close(index);
                        });
                        return;
                    }
                    layer.msg(message);
                })
                return false;
            });
        });
    }

    goHome = () => {
        let {history} = this.props;
        history.replace("/message");
    }

    render() {
        return (
            <div className="layui-row">
                <div className="layui-card vi-padding">
                    <div className="layui-card-header ">
                        <div className="layui-row">
                                <strong className="vi-point" onClick={this.goHome}>
                                    <i className="layui-icon">&#xe65c;</i>
                                </strong>&nbsp;&nbsp;&nbsp;
                            <i className="layui-icon">&#xe618;</i> 邮箱注册
                        </div>
                    </div>
                    <div className="layui-card-body">
                            <form className="layui-row layui-form layui-form-pane">
                                <div className="layui-col-sm6 layui-col-sm-offset3">
                                    <div className="layui-row">
                                        <blockquote className="layui-elem-quote layui-quote-nm">
                                            海子说：远方除了遥远、一无所有。
                                        </blockquote>
                                    </div>
                                    <div className="layui-form-item">
                                        <label className="layui-form-label">昵称</label>
                                        <div className="layui-input-block">
                                            <input type="text" name="nickName" required  lay-verify="required" placeholder="起一个好听的昵称吧"
                                                   autoComplete="off" className="layui-input"/>
                                        </div>
                                    </div>
                                    <div className="layui-form-item">
                                        <label className="layui-form-label">邮箱</label>
                                        <div className="layui-input-block">
                                            <input type="text" name="email"  ref="code" required lay-verify="required|email" placeholder="推荐使用qq邮箱"
                                                   autoComplete="off" className="layui-input"/>
                                        </div>
                                    </div>
                                    <div className="layui-form-item">
                                        <div className="layui-inline">
                                            <label className="layui-form-label">验证码</label>
                                            <div className="layui-input-block">
                                                <input type="text" name="code"
                                                       required
                                                        lay-verify="required" placeholder="请输入邮箱验证码"
                                                       autoComplete="off" className="layui-input"/>
                                            </div>
                                        </div>
                                        <div className="layui-inline">
                                            <button className="layui-btn layui-btn-primary" onClick={this.sendEmailCode}>发送邮箱验证码</button>
                                        </div>
                                    </div>
                                    <div className="layui-form-item">
                                        <textarea name="content"  lay-verify="required" required placeholder="请输入你的留言（不超过300字）"
                                                  className="layui-textarea"></textarea>
                                    </div>
                                    <div className="layui-form-item">
                                        <button className="layui-btn"
                                                onClick={this.register}
                                                lay-submit="true" lay-filter="register">注册</button>
                                        <button type="reset" className="layui-btn layui-btn-primary">重置</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;