import React, {Component} from 'react';
import axios from "axios";

class ClassificationCard extends Component {


    findBlogByTypeId = (id, blogCount) => {
        if (blogCount <= 0) {
            /*global layer*/
            layer.msg("抱歉、该分类暂无博文。");
            return;
        }
        let {updateBlogDescription} = this.props;
        axios("/types/blogs/" + id).then(response => {
            updateBlogDescription(response.data.data.blogList);
        })
    }

    render() {
        let {types} = this.props;
        return (
            <div className="layui-row">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe60a;</i> 分类
                    </div>
                    <div className="layui-card-body">
                        <div className="layui-row vi-bottom">
                            <div className="layui-btn-container">
                                {
                                    types.map(item => {
                                        return (
                                            <button type="button" key={item.id}
                                                    onClick={() => this.findBlogByTypeId(item.id, item.blogCount)}
                                                    className="layui-btn layui-btn-radius layui-btn-primary">
                                                {item.typeName}
                                                <span className="layui-badge layui-bg-cyan">{item.blogCount}</span>
                                            </button>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ClassificationCard;