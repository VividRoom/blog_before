import React, {Component} from 'react';
import "./TagCard.css";
import axios from "axios";

class TagCard extends Component {

    updateBlogDescription = (id, blogCount) => {
        if (blogCount <= 0) {
            /*global layer*/
            layer.msg("抱歉、该标签暂无博文。");
            return;
        }
        let {updateBlogDescription} = this.props;
        axios("/tags/blogs/" + id).then(response => {
            updateBlogDescription(response.data.data.blogList);
        })
    }


    render() {
        let {tags} = this.props;
        return (
            <div className="layui-row">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe66e;</i> 标签
                    </div>
                    <div className="layui-card-body">
                        <div className="layui-row  vi-bottom">
                            <div className="layui-btn-container">
                                {
                                    tags.map(item => {
                                        return (
                                            <button type="button" key={item.id}
                                                    onClick={() => this.updateBlogDescription(item.id, item.blogCount)}
                                                    className="layui-btn layui-btn-primary layui-btn-radius">
                                                {item.tagName}
                                                <span className="layui-badge layui-bg-cyan">{item.blogCount}</span>
                                            </button>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TagCard;