import React, {Component} from 'react';
import axios from "axios";
import {Link} from "react-router-dom"

class Recommend extends Component {

    state = {
        simpleBlog: [],
    }

    componentDidMount() {
        axios("/blogs/time").then(response => {
            this.setState({simpleBlog: response.data.data});
        })
    }

    render() {
        let {simpleBlog} = this.state;
        return (
            <div className="layui-row vi-top">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe705;</i>
                        &nbsp;最新推荐
                    </div>
                    <div className="layui-card-body">
                        {
                            simpleBlog.map(item => {
                                return (
                                    <div key={item.id} className="layui-row vi-bottom vi-recommend">
                                        <Link to={"/search/" + item.id}>{item.title}</Link>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Recommend;