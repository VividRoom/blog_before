import React, {Component} from 'react';
import ArticleList from "./ArticleList/ArticleList";
import TagCard from "./TagCard/TagCard";
import Recommend from "./Recommend/Recommend";
import ClassificationCard from "./ClassificationCard/ClassificationCard";
import axios from "axios";
import img4 from "./img/4.jpg"
import img5 from "./img/5.jpg"
import img6 from "./img/6.jpg"
import img7 from "./img/7.jpg"

class Home extends Component {

    state = {
        blogDescription: [],
        tags: [],
        types: [],
        total: 0
    }


    updateAll = () => {
        axios("/blogs/page/1/7").then((response) => {
            let {total, list} = response.data.data;
            this.setState({total,  blogDescription: list || []});
        });
    }

    componentDidMount() {
        /*global layui*/
        layui.use('carousel', function () {
            var carousel = layui.carousel;
            //建造实例
            carousel.render({
                elem: '#test1',
                width: "100%",
                height: "500px",
                arrow: 'always',
                interval: 7000
            });
        });

        this.updateAll()

        axios("/tags?page=1&limit=7").then(response => {
            this.setState({tags: response.data.data.list});
        });

        axios("/types?page=1&limit=5").then((response) => {
            this.setState({types: response.data.data.list});
        });
    }

    updateBlogDescription = (blogDescription) => {
        this.setState({blogDescription})
    }

    render() {
        let {types, tags, blogDescription, total} = this.state;
        let {history} = this.props;
        return (
            <>
                <div className="layui-row">
                    <div className="layui-carousel" id="test1">
                        <div carousel-item="true">
                            <div><img src={img4} alt="从明天起、做一个幸福的人"/></div>
                            <div><img src={img5} alt="陌生人、我也为你祝福"/></div>
                            <div><img src={img6} alt="愿你有情人终成眷属"/></div>
                            <div><img src={img7} alt="愿你在尘世获得幸福"/></div>
                        </div>
                    </div>
                </div>
                <div className="layui-col-sm8">
                    <ArticleList total={total} blogDescription={blogDescription} updateBlogDescription={this.updateBlogDescription}/>
                </div>
                <div className="layui-col-sm4">
                    <ClassificationCard types={types} updateBlogDescription={this.updateBlogDescription}/>
                    <div className="vi-top">
                        <TagCard tags={tags} updateBlogDescription={this.updateBlogDescription}/>
                    </div>
                    <Recommend history={history}/>
                </div>
            </>
        );
    }
}

export default Home;