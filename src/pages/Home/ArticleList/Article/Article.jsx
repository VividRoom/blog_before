import React, {Component} from 'react';
import "./Article.css";
import {Link} from "react-router-dom"

class Article extends Component {
    render() {
        let {id, title, description, type, createTime, viewCount, firstPicture} = this.props;
        return (
            <div className="layui-row layui-col-space5 vi-bottom-green">
                <div className="layui-col-sm7">
                    <Link className="vi-header-font" to={"/search/" + id}>{title}</Link>
                    <p className="layui-text ">
                        {description}
                    </p>
                    <div className="layui-row vi-top">
                        <span className="layui-badge layui-bg-green">{type.typeName}</span>
                        <span className="vi-left">
                                   <i className="layui-icon">&#xe637;</i>
                            &nbsp;<span>{new Date(createTime).toLocaleString()}</span>
                               </span>
                        <span className="vi-left">
                                   <i className="layui-icon">&#xe635;</i>&nbsp;&nbsp;
                            <span>{viewCount}</span>
                               </span>
                    </div>
                </div>
                <div className="layui-col-sm5">
                    <img src={firstPicture} className="layui-circle" width="100%" alt="hello world"/>
                </div>
            </div>
        );
    }
}

export default Article;