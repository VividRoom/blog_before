import React, {Component} from 'react';
import Article from "./Article/Article";
import axios from "axios";

class ArticleList extends Component {

    state = {
        pages: 0,
        page: 1,
        limit: 5,
        hasPreviousPage: false,
        hasNextPage: false,
    }

    updateAll = () => {
        let {page, limit} = this.state;
        axios("/blogs/page/" + page + "/" + limit).then((response) => {
            let {pages, hasPreviousPage, hasNextPage} = response.data.data;
            this.setState({pages, hasPreviousPage, hasNextPage});
        });
    }

    componentDidMount() {
        this.updateAll();
    }

    prePage = () => {
        let {hasPreviousPage} = this.state;
        if (!hasPreviousPage) {
            layer.msg("抱歉、没有上一页。");
            return;
        }
        this.updatePrePage();
    }

    nextPage = () => {
        let {hasNextPage} = this.state;
        if (!hasNextPage) {
            /*global layer*/
            layer.msg("抱歉、没有下一页。");
            return;
        }
        this.updateNextPage();
    }

    updatePrePage = () => {
        let {page, limit} = this.state;
        if (page <= 0) {
            return;
        }
        page = page - 1;
        this.setState({page: page});
        let {updateBlogDescription} = this.props;
        axios("/blogs/page/" + page + "/" + limit).then((response) => {
            let {list, pages, hasPreviousPage, hasNextPage} = response.data.data;
            updateBlogDescription(list);
            this.setState({pages,  hasPreviousPage, hasNextPage});
        });
    }

    updateNextPage = () => {
        let {page, pages, limit} = this.state;
        if (page > pages) {
            return;
        }
        page = page + 1;
        this.setState({page: page});

        let {updateBlogDescription} = this.props;
        axios("/blogs/page/" + page + "/" + limit).then((response) => {
            let {list, pages, hasPreviousPage, hasNextPage} = response.data.data;
            updateBlogDescription(list);
            this.setState({pages, hasPreviousPage, hasNextPage});
        });
    }

    render() {
        let {blogDescription, total} = this.props;
        let {hasPreviousPage, hasNextPage} = this.state;
        return (
            <div className="layui-card">
                <div className="layui-card-header">
                    <div className="layui-row">
                        <div className="layui-col-sm5">
                            <i className="layui-icon">&#xe609;</i> Vivi的博客
                        </div>
                        <div className="layui-col-sm4 text-right layui-col-sm-offset3 layui-text"
                             style={{paddingTop: "11px"}}>
                            本站共 <span>{total}</span> 条博文
                        </div>
                    </div>
                </div>
                <div className="layui-card-body">
                    {
                        blogDescription.map((item) => {
                            return (<Article key={item.id} {...item}/>)
                        })
                    }
                    <div className="layui-row vi-top ">
                        <div className="layui-col-sm6 text-left">
                            <button
                                onClick={this.prePage}
                                className={hasPreviousPage ? 'layui-btn layui-btn-radius layui-btn-primary' : 'layui-btn layui-btn-radius layui-btn-primary layui-disabled'}>
                                <i className="layui-icon ">&#xe65a;</i>
                                上一页
                            </button>
                        </div>
                        <div className="layui-col-sm6 text-right">
                            <button
                                onClick={this.nextPage}
                                className={hasNextPage ? "layui-btn layui-btn-radius layui-btn-primary" : "layui-btn layui-btn-radius layui-btn-primary layui-disabled"}>
                                下一页
                                <i className="layui-icon">&#xe65b;</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ArticleList;