import React, {Component} from 'react';
import ArticleList from "../Home/ArticleList/ArticleList";
import TagCard from "../Home/TagCard/TagCard";
import axios from "axios";

class TagsPage extends Component {

    state = {
        blogDescription: [],
        tags: [],
        total: 0
    }

    updateAll = () => {
        axios("/blogs/page/1/7").then((response) => {
            let {total, list, pages} = response.data.data;
            this.setState({total, pages, blogDescription: list});
        });
    }

    componentDidMount() {

        this.updateAll();

        axios("/tags/all").then(response => {
            this.setState({tags: response.data.data});
        });
    }

    updateBlogDescription = (blogDescription) => {
        this.setState({blogDescription})
    }

    render() {
        let {tags, blogDescription, total} = this.state;
        return (
            <>
                <div className="layui-col-sm8">
                    <ArticleList total={total} blogDescription={blogDescription} updateBlogDescription={this.updateBlogDescription}/>
                </div>
                <div className="layui-col-sm4">
                    <TagCard tags={tags} updateBlogDescription={this.updateBlogDescription}/>
                </div>
            </>
        );
    }
}

export default TagsPage;