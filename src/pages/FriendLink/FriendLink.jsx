import React, {Component} from 'react';
import "./FriendLink.css"
import axios from "axios";

class FriendLink extends Component {

    state = {
        links: []
    }

    componentDidMount() {
        axios("/links?page=1&limit=7").then(response => {
            let {list} = response.data.data;
            this.setState({links: list});
        })
    }

    render() {
        let {links} = this.state;
        return (
            <>
            <div className="layui-row">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe64c;</i>友情链接
                    </div>
                </div>

                <div className="layui-row layui-col-space30">
                    {
                        links.map(item => {
                            return (
                                <div className="layui-col-sm4" key={item.id}>
                                    <div className="layui-card shadow">
                                        <div className="layui-card-body ">
                                            <a href={item.url} style={{display: "block"}} target="_blank">
                                                <img src={item.img} width="100%"/>
                                                <div className="layui-row">
                                                    <span style={{fontSize: "17px"}}>{item.title}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
    </>
    )
        ;
    }
}

export default FriendLink;