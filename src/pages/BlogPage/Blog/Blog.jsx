import React, {Component} from 'react';
// import ReactMarkdown from 'react-markdown'
import marked from 'marked';
import "./Blog.css";
import Catalog from "../Catalog/Catalog";


class Blog extends Component {

    state = {
        content: "",
    }

    componentDidMount() {
        this.highlightCallBack();
    }

    componentDidUpdate() {
        this.highlightCallBack();
    }


    highlightCallBack = () => {
        document.querySelectorAll("pre code").forEach(block => {
            try {
                /*global hljs*/
                hljs.highlightBlock(block);
            } catch (e) {
                console.log(e);
            }
        });
    };

    render() {
        let {content, flag, type, firstPicture, title} = this.props;
        return (
            <>
                <div className="layui-row layui-col-space15">
                    <div className="layui-col-sm10">
                        <div className="layui-row">
                            <img src={firstPicture} alt="" width="100%"/>
                        </div>
                        <div className="layui-row vi-top vi-bottom">
                            <div className="layui-col-sm4 layui-col-lg-offset8 text-right">
                                <span className="layui-badge layui-bg-green">{flag}</span>
                            </div>
                        </div>
                        <div className="layui-row vi-top">
                            <blockquote className="layui-elem-quote layui-quote-nm">{title}</blockquote>
                        </div>
                        <div className="layui-row layui-text vi-padding js-toc-content" style={{fontSize: "14px"}}>
                            {/*  <ReactMarkdown
                        source={content}
                        escapeHtml={false}  //不进行HTML标签的转化
                    />*/}
                            <div dangerouslySetInnerHTML={{__html: content}} ></div>

                        </div>
                        <div className="layui-row vi-padding">
                            <button type="button" className="layui-btn layui-btn-sm layui-btn-radius">{type}</button>
                        </div>
                    </div>

                    <div className="layui-col-sm2">
                        <Catalog/>
                    </div>
                </div>
            </>
        );
    }
}

export default Blog;