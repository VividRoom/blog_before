import React, {Component} from 'react';
import Blog from "./Blog/Blog";
import axios from "axios";
import marked from 'marked';

class BlogPage extends Component {

    state = {
        blog: {},
        type: {}
    }

    componentDidMount() {
        let {id} = this.props.match.params;
        axios("/blogs/" + id).then(response => {
            let {data} = response.data;
            this.setState({blog: data, type: data.type});
        })
    }


    goHome = () => {
        let {history} = this.props;
        history.replace("/home");
    }

    render() {
        let {updateTime, viewCount, title, content, flag, firstPicture, createTime, appreciateStatus} = this.state.blog;
        let {typeName} = this.state.type;
        return (
            <div className="layui-card">
                <div className="layui-card-header">
                    <strong className="vi-point" onClick={this.goHome}>
                        <i className="layui-icon">&#xe65c;</i>
                    </strong>
                    <strong className="vi-left">
                        <i className="layui-icon">&#xe609;</i>&nbsp;
                        <span>Vivi</span>
                    </strong>
                    <span className=" layui-text vi-left">
                <span>更新于：</span>
                <span>
                    <i className="layui-icon">&#xe637;</i>&nbsp;
                    <span>{new Date(updateTime).toLocaleString()}</span>
                </span>
            </span>
                    <span className="layui-text vi-left">
                <span>浏览：</span>
                <span>
                    <i className="layui-icon">&#xe635;</i>&nbsp;
                    {viewCount}
                </span>
            </span>
                </div>
                <div className="layui-card-body">
                    {/*博文*/}
                    <Blog title={title} content={ marked(content || "## hello world")} flag={flag} type={typeName} firstPicture={firstPicture}/>
                    <div className={appreciateStatus ? "layui-row" : "layui-hide"}>
                        <div className="layui-col-sm4 layui-col-sm-offset4">
                            <button type="button" disabled="disabled"
                                    className="layui-btn layui-btn-danger layui-btn-fluid layui-btn-radius">
                                <i className="layui-icon">&#xe65e;</i>&nbsp;赞赏
                            </button>
                        </div>
                    </div>
                    <div className="layui-row vi-padding layui-text">
                        <li>作者：Vivi</li>
                        <li>发表时间：{new Date(createTime).toLocaleString()}</li>
                        <li>版权声明：自由转载</li>
                    </div>
                    <div className="layui-row vi-padding layui-text">
                        <div className="layui-col-sm10">
                            <blockquote className="layui-elem-quote">评论功能已关闭、请在留言板留言</blockquote>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BlogPage;