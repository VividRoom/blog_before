import React, {Component} from 'react';

class Comment extends Component {
    render() {
        return (
            <fieldset className="layui-elem-field">
                <legend>评论列表</legend>
                <div className="layui-field-box">
                    <div className="layui-form layui-form-pane">
                                <textarea name="" required lay-verify="required"
                                          placeholder="请输入评论内容"
                                          className="layui-textarea">
                                </textarea>
                        <div className="layui-row vi-top">
                            <div className="layui-inline">
                                <div className="layui-form-item">
                                    <label className="layui-form-label">用户名</label>
                                    <div className="layui-input-block">
                                        <input type="text" name="title" required lay-verify="required"
                                               placeholder="请输入用户名" autoComplete="off"
                                               className="layui-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="layui-inline">
                                <div className="layui-form-item">
                                    <label className="layui-form-label">邮箱</label>
                                    <div className="layui-input-block">
                                        <input type="text" name="title" required lay-verify="email"
                                               placeholder="请输入邮箱" autoComplete="off"
                                               className="layui-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="layui-inline">
                                <div className="layui-form-item">
                                    <button type="button" lay-submit="true" className="layui-btn">
                                        <i className="layui-icon">&#xe681;</i>&nbsp;
                                        发布
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        );
    }
}

export default Comment;