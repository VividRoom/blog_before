import React, {Component} from 'react';
import ArticleList from "../Home/ArticleList/ArticleList";
import ClassificationCard from "../Home/ClassificationCard/ClassificationCard";
import axios from "axios";

class ClassificationPage extends Component {
    state = {
        blogDescription: [],
        types: [],
        total: 0
    }

    updateAll = () => {
        axios("/blogs/page/1/7").then((response) => {
            let {total, list, pages} = response.data.data;
            this.setState({total, pages, blogDescription: list});
        });
    }

    componentDidMount() {
        this.updateAll();

        axios("/types/all").then((response) => {
            this.setState({types: response.data.data});
        });
    }

    updateBlogDescription = (blogDescription) => {
        this.setState({blogDescription})
    }

    render() {
        let {types, blogDescription, total} = this.state;
        return (
            <>
                <div className="layui-col-sm8">
                    <ArticleList total={total} blogDescription={blogDescription} updateBlogDescription={this.updateBlogDescription}/>
                </div>
                <div className="layui-col-sm4">
                    <ClassificationCard types={types} updateBlogDescription={this.updateBlogDescription}/>
                </div>
            </>
        );
    }
}

export default ClassificationPage;