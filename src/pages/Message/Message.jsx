import React, {Component} from 'react';
import MessageCard from "./MessageCard/MessageCard";
import {Link} from "react-router-dom";
import axios from "axios";

class Message extends Component {

    state = {
        message: [],
        count: 0
    }

    componentDidMount() {
        axios("/messages?page=1&limit=7").then(response => {
            this.setState({message: response.data.data.list});
        })

    }


    sendMessage = () => {
        let {history} = this.props;
        let messageObj = this;
        let {count} = this.state;
        count = count + 1;
        this.setState({count});
        /*global layui*/
        layui.use(['form', 'layer'], function(form, layer){

            form.on('submit(sendMessage)', function(data){

                let {count} = messageObj.state;
                if (count >= 5) {
                    layer.alert("抱歉、留言发送过于频繁、请稍后再试！！");
                    return false;
                }

                let {content} = data.field;
                if (content.trim().length > 300) {
                    layer.alert("抱歉、留言字数过多、请精简！", {icon: 5, title: false});
                    return false;
                }
                axios.post("/messages/", data.field).then(response => {
                    let {status, message} = response.data;
                    if (!status) {
                        layer.confirm(message, {icon: 6, title: false}, function (index) {
                            history.push("/register");
                            layer.close(index);
                        });
                        return;
                    }
                    layer.alert("发布成功~~", {icon: 6, title: false}, function (index) {
                        messageObj.updateMessage();
                        form.val("messageForm", {nickName: "", content: "", email: ""});
                        layer.close(index);
                    });
                })
                return false;
            });
        });
    }

    updateMessage = () => {
        axios("/messages?page=1&limit=7").then(response => {
            this.setState({message: response.data.data.list});
        });
    }

    render() {
        let {message} = this.state;
        return (
            <div className="layui-row">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <i className="layui-icon">&#xe6b2;</i> 留言
                    </div>
                    <div className="layui-card-body">

                        <div className="layui-row layui-text">
                            <blockquote className="layui-elem-quote layui-quote-nm">
                                我有一所房子、面朝大海、春暖花开。<Link to={"/register/"}>未注册邮箱？去注册</Link>
                            </blockquote>
                        </div>

                        <fieldset className="layui-elem-field">
                            <legend>留言面板</legend>
                            <div className="layui-field-box">
                                <form lay-filter="messageForm" className="layui-row layui-form layui-form-pane">
                                    <textarea name="content" required lay-verify="required" placeholder="温馨提示：留言前需注册邮箱"
                                              className="layui-textarea"></textarea>
                                    <div className="layui-form-item vi-top">
                                        <div className="layui-inline">
                                            <div className="layui-form-item">
                                                <label className="layui-form-label">昵称</label>
                                                <div className="layui-input-block">
                                                    <input type="text" name="nickName" required lay-verify="required"
                                                           placeholder="请输入你的昵称" autoComplete="off"
                                                           className="layui-input"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="layui-inline">
                                            <div className="layui-form-item">
                                                <label className="layui-form-label">邮箱</label>
                                                <div className="layui-input-block">
                                                    <input type="text" name="email" required lay-verify="required|email"
                                                           placeholder="请输入你的邮箱" autoComplete="off"
                                                           className="layui-input"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="layui-inline">
                                            <div className="layui-form-item">
                                             <button className="layui-btn" lay-submit="true" lay-filter="sendMessage"
                                             onClick={this.sendMessage}
                                             >发布留言</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </fieldset>
                        {
                            message.map(item => {
                                return (
                                    <MessageCard key={item.id} {...item}/>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Message;