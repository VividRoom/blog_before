import React, {Component} from 'react';

class MessageCard extends Component {
    render() {
        let {nickName, content, createTime, email} = this.props;
        return (
            <div className="layui-row">
                <div className="layui-card">
                    <div className="layui-card-header">
                        <strong>
                            <i className="layui-icon">&#xe687;</i>
                            {nickName}
                        </strong>
                    </div>
                    <div className="layui-card-body layui-text">
                        <div className="layui-row">
                            {content}
                        </div>
                        <div className="layui-row text-right">
                            <strong>
                                <i className="layui-icon">&#xe68d;</i>
                                {new Date(createTime).toLocaleString()}
                            </strong>
                            <strong className="vi-left">
                                <i className="layui-icon">&#xe618;</i>
                                {email}
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MessageCard;