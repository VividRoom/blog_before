import React, {Component} from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import Home from "../../pages/Home/Home";
import TagsPage from "../../pages/TagsPage/TagsPage";
import BlogPage from "../../pages/BlogPage/BlogPage";
import ClassificationPage from "../../pages/ClassificationPage/ClassificationPage";
import TimeLinePage from "../../pages/TimeLinePage/TimeLinePage";
import AboutMe from "../../pages/AboutMe/AboutMe";
import Search from "../../pages/Search/Search";
import Message from "../../pages/Message/Message";
import Register from "../../pages/Register/Register";
import FriendLink from "../../pages/FriendLink/FriendLink";

class Container extends Component {


    render() {
        return (
            <div  className="layui-container vi-top">
                <div className="layui-row layui-col-space28">
                    <Switch>
                        <Route path="/home" component={Home}/>
                        <Route path="/tags" component={TagsPage}/>
                        <Route path="/search/:id" component={BlogPage}/>
                        <Route path="/classes" component={ClassificationPage}/>
                        <Route path="/timeline" component={TimeLinePage}/>
                        <Route path="/message" component={Message}/>
                        <Route path="/register" component={Register}/>
                        <Route path="/about" component={AboutMe}/>
                        <Route path="/links" component={FriendLink}/>
                        <Route path="/likeSearch" component={Search}/>
                        <Redirect to="/home"/>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default Container;