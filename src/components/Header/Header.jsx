import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <ul className="layui-nav">
                <li className="layui-nav-item">
                    <Link to="/">
                        <i className="layui-icon">&#xe609;</i> Vivi的博客
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/home">
                        <i className="layui-icon layui-icon-home"> </i> 首页
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/tags">
                        <i className="layui-icon">&#xe66e;</i> 标签
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="classes">
                        <i className="layui-icon">&#xe60a;</i> 分类
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/timeline">
                        <i className="layui-icon">&#xe62c;</i> 时间线
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/about">
                        <i className="layui-icon">&#xe60b;</i> 关于
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/message">
                        <i className="layui-icon">&#xe6b2;</i> 留言
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link to="/links">
                        <i className="layui-icon">&#xe64c;</i> 友链
                    </Link>
                </li>
                <li className="layui-nav-item">
                    <Link  to="/likeSearch">
                        {/*font-weight: bold; font-size: 17px*/}
                        <i className="layui-icon" style={{fontWeight: "bold", fontSize: "17px"}}>&#xe615;</i>
                    </Link>
                </li>
            </ul>
        );
    }
}

export default Header;